var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
        console.log("besede pred shiftom: " + besede);
      besede.shift();
        console.log("besede po shiftu: " + besede);
      besede = besede.join(' ');
        console.log("besede po joinu: " + besede);
      if(besede.indexOf('"') == -1) {
        this.spremeniKanal(besede);
      } else {
        besede = besede.split('"');
          console.log("besede po splitu po narekovaju: " + besede);
        var kanal = besede[1];
        var geslo = besede[3];
        this.socket.emit('pridruzitevGesloZahteva', {kanal: kanal, geslo: geslo});
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var komuTmp = besede.shift();
      var komu = komuTmp.substring(1, komuTmp.length-1);
      var vsebinaTmp = besede.join(' ');
      var vsebina = vsebinaTmp.substring(1, vsebinaTmp.length-1);
      this.socket.emit('zasebnoSporocilo', {besedilo: vsebina, komu: komu});
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };
  
  return sporocilo;
};