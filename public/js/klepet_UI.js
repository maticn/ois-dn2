function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

// replaceAll stackoverflow
function replaceAll(find, replace, string) {
  while(string.indexOf(find) != -1) {
    string = string.replace(find, replace);
  }
  return string;
}

// Create our array (parsanje XML-ja)
  var arrWord = new Array();

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    var prejetNizKanal = $('#kanal').text();
    var kanalSplit = prejetNizKanal.split('@');
    var imeKanalaPresledek = kanalSplit[1];
    var imeKanala = imeKanalaPresledek.substring(1, imeKanalaPresledek.length);
    if(sporocilo.indexOf('<script>alert') != -1) {
      klepetApp.posljiSporocilo(imeKanala, sporocilo);
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
      console.log('alert posiljam');
    } else if(sporocilo.indexOf(';)') != -1 || sporocilo.indexOf(':)') != -1 || sporocilo.indexOf('(y)') != -1 || sporocilo.indexOf(':*') != -1 || sporocilo.indexOf(':(') != -1) {
      sporocilo = replaceAll(';)', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png" class="image">', sporocilo);
      sporocilo = replaceAll(':)', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png" class="image">', sporocilo);
      sporocilo = replaceAll('(y)', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png" class="image">', sporocilo);
      sporocilo = replaceAll(':*', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png" class="image">', sporocilo);
      sporocilo = replaceAll(':(', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png" class="image">', sporocilo);
      
      klepetApp.posljiSporocilo(imeKanala, sporocilo);
      $('#sporocila').append(divElementHtmlTekst(sporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
      console.log('smesko');
    }
    else {
      //  Naloga 2.3 - Filter vulgarnih besed
      var dolTrenBes;
      var zvezdice = "";
      var sporociloRazbij = sporocilo.split("  ");
      for(var k=0; k<sporociloRazbij.length; k++) {
        for(var i in arrWord) {
          if(sporociloRazbij[k] === arrWord[i]) {
            console.log(i); // pozicija "swear" besede v tabeli, zacensi z 0
            dolTrenBes = arrWord[i].length;
            console.log('dolzina grde besede ' + dolTrenBes);
            
            var j = 0;
            while(j < dolTrenBes) {
              zvezdice = zvezdice + "*";
              j++;
            }
            sporociloRazbij[k] = zvezdice;
          }
        }
      }
      sporocilo = sporociloRazbij.join(" ");
      
      klepetApp.posljiSporocilo(imeKanala, sporocilo);
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
      console.log('plain besedilo');
      }
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  //  parsanje XML dokumenta : w3schools.com
    // Create our array
    // var arrWord = new Array();
    
    if (window.XMLHttpRequest) {
      xhttp=new XMLHttpRequest();
    }
    else { // code for IE5 and IE6
      xhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.open("GET","../swearWords.xml",false);
    xhttp.send();
    xmlDoc=xhttp.responseXML;
    
    var x=xmlDoc.getElementsByTagName("word");
    
    for (i=0; i<x.length; i++) {
      arrWord.push(x[i].childNodes[0].nodeValue);
      console.log(x[i].childNodes[0].nodeValue);
      //console.log("<br>");
    }
  //  konec parsanja XML dokumenta
  
  /*
  //  parsanje swearWords.txt
  $.get("../swearWords.txt", function (response) {
        swearWordsVtabelo(response);
        console.log(response);
  });
  */
  
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  socket.on('pridruzitevGesloNOK', function(kanal) {
    var novElement = $('<div style="font-weight: bold"></div>').text("Pridružitev v kanal " + kanal + " ni bilo uspešno, ker je geslo napačno!");
    $('#sporocila').append(novElement);
  });
  
  socket.on('pridruzitevGesloBREZ', function(kanal) {
    var novElement = $('<div style="font-weight: bold"></div>').text("Izbrani kanal " + kanal + " je prosto dostopen in ne zahteva prijave z geslom, " + 
    "zato se prijavite z uporabo /pridruzitev " + kanal + " ali zahtevajte kreiranje kanala z drugim imenom.");
    $('#sporocila').append(novElement);
  });

  socket.on('sporocilo', function (sporocilo) {
    if(sporocilo.besedilo.indexOf('<script>alert') != -1) {
      var novElement = $('<div style="font-weight: bold"></div>').append(divElementEnostavniTekst(sporocilo.besedilo));
      $('#sporocila').append(novElement);
      console.log('ALLLLLLLERT');
    } else {
      var novElement = $('<div style="font-weight: bold"></div>').append(divElementHtmlTekst(sporocilo.besedilo));
      $('#sporocila').append(novElement);
    }
  });

  socket.on('zsNapaka', function (sporociloBesedilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text("Sporočila " + sporociloBesedilo.besedilo + " uporabniku z vzdevkom " +
    sporociloBesedilo.komu + " ni bilo mogoče posredovati.");
    $('#sporocila').append(novElement);
  });

  socket.on('zsTrue', function (sporociloBesedilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text("(zasebno za " + sporociloBesedilo.komu + "): " + sporociloBesedilo.besedilo);
    $('#sporocila').append(novElement);
    });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  socket.on('seznamUserjevReply', function(serverVir) {
    $('#seznam-uporabnikov').empty();
    for (var i in serverVir.seznamUserjev) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(serverVir.seznamUserjev[i]));
    }
  });
  
  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});