var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var kanaliZgeslom = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebnoSporocilo(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    seznamUporabnikov(trenutniKanal[socket.id]);
  });
};

function seznamUporabnikov(kanal) {
  var seznamUserjev = [];
  for (var i in io.sockets.clients(kanal)) {  // vrne array klientov na kanalu
    seznamUserjev.push(vzdevkiGledeNaSocket[io.sockets.clients(kanal)[i].id]);  // id kaze na vzdevek uporabnika
  }
  io.sockets.to(kanal).emit('seznamUserjevReply', {
    seznamUserjev: seznamUserjev
  });
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal, vzdevek: vzdevkiGledeNaSocket[socket.id]});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: trenutniKanal[socket.id]
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
    // ponovno obdelaj uporabnike zaradi spremembe vzdevka
    seznamUporabnikov(trenutniKanal[socket.id]);
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajZasebnoSporocilo(socket) {
  socket.on('zasebnoSporocilo', function (sporocilo) {
    if(uporabljeniVzdevki.indexOf(sporocilo.komu) != -1 && (sporocilo.komu !== vzdevkiGledeNaSocket[socket.id])) {
      for(var i in vzdevkiGledeNaSocket) {
        if(vzdevkiGledeNaSocket[i] === sporocilo.komu) {
          // poslje sporocilo naslovniku
          io.sockets.socket(i).emit('sporocilo', {besedilo: vzdevkiGledeNaSocket[socket.id] + " (zasebno): " + sporocilo.besedilo});
          // obvesti posiljatelja o uspesnosti
          io.sockets.socket(socket.id).emit('zsTrue', {besedilo: sporocilo.besedilo, komu: sporocilo.komu});
        }
      }
    } else {
      socket.emit('zsNapaka', sporocilo);
    }
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    var kanalKjerOdhajam = trenutniKanal[socket.id];
    var kanali = Object.keys(kanaliZgeslom);
    if(kanali.indexOf(kanal.novKanal) === -1) {
      socket.leave(trenutniKanal[socket.id]);
      seznamUporabnikov(kanalKjerOdhajam);
      pridruzitevKanalu(socket, kanal.novKanal);
      seznamUporabnikov(trenutniKanal[socket.id]);
    } else {
      io.sockets.socket(socket.id).emit('pridruzitevGesloNOK', kanal.novKanal);
    }
  });
  socket.on('pridruzitevGesloZahteva', function(vhod) {
    var kanali = Object.keys(kanaliZgeslom);  // dobi vse kljuce tega objekta ; torej vse kanale, zascitene z geslom (npr. Skedenj)
    var iKanala = kanali.indexOf(vhod.kanal);
    if(iKanala === -1) {
      kanaliZgeslom[vhod.kanal] = vhod.geslo; // hkrati shranim [kanal] (npr. [pivnica]) v objekt in mu dodam geslo (npr. pivo)
      zamenjajKanal(socket, vhod.kanal);
      seznamUporabnikov(trenutniKanal[socket.id]);
    } else {
      if(kanaliZgeslom[kanali[iKanala]] === vhod.geslo) {  // zgolj s kanali[indexKanala] bi preverjal ime kanala, ne pa njegovega gesla
        zamenjajKanal(socket, vhod.kanal);
        seznamUporabnikov(trenutniKanal[socket.id]);
      } else {
        io.sockets.socket(socket.id).emit('pridruzitevGesloNOK', vhod.kanal);
      }
    }
  });
}

function zamenjajKanal(socket, kanal) {
  var kanalKjerOdhajam = trenutniKanal[socket.id];
  socket.leave(trenutniKanal[socket.id]);
  seznamUporabnikov(kanalKjerOdhajam);
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal, vzdevek: vzdevkiGledeNaSocket[socket.id]});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
  socket.on('disconnect', function() {
      socket.leave(trenutniKanal[socket.id]);     // ko nekdo zapusti kanal
      seznamUporabnikov(trenutniKanal[socket.id]);// zopet obdelaj uporabnike
    })
}